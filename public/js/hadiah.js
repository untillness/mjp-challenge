let cropImage;
const imgCrop = document.getElementById("img-crop");
const opCropImg = {
    responsive: true,
    viewMode: 2,
    aspectRatio: 16 / 9,

    minContainerWidth: 466.021,
    minContainerHeight: 466.021,
};

let idImgPreview,
    idImg = "";

const getDataHadiah = () => {
    $.ajax({
        method: "GET",
        url: __API_URL + "get-all",
        beforeSend: () => {
            let element = "";
            element = `
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    Memuat ....
                                </div>
                            </div>
                        </div>
                    `;
            $("#daftar-hadiah").html(element);

            NP.s();
        },
        success: function (data) {
            if (data.status == 1) {
                let dataHadiah = data.data;
                let element = "";

                if (dataHadiah.length > 0) {
                    dataHadiah.forEach((node, i) => {
                        element += buildCard(node);
                    });
                } else {
                    element = `
                        <div class="col-12">
                            <div class="card bg-primary">
                                <div class="card-body">
                                    Tidak ada data hadiah untuk saat ini.
                                </div>
                            </div>
                        </div>
                    `;
                }
                $("#daftar-hadiah").html(element);
            }

            NP.d();
        },
    });
};

const buildCard = (node) => {
    return `
        <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-5 col-md-5 col-12 mb-3 mb-md-0 text-center" style="display: flex;flex-direction: column;justify-content: center;">
                                <img src="${__BASE_URL + node.img}" alt=""
                                    class="img-fluid img-responsive img-rounded">
                            </div>
                            <div class="col-sm-7 col-md-7 col-12">
                                <h3 class="text-bold">${node.nama}</h3>
                                    <div class="mt-2">
                                        <div class="badge badge-primary">${
                                            node.poin
                                        } POIN</div>
                                        <div class="badge badge-success">STOK ${
                                            node.stok
                                        }</div>
                                    </div>
                                <hr>
                                <div class="btn-group">
                                    <button class="btn btn-warning btn-sm" onclick="editHadiah(${
                                        node.id
                                    })"><i
                                            class="fas fa-edit"></i></button>
                                    <button class="btn btn-danger btn-sm" onclick="deleteHadiah(${
                                        node.id
                                    })"><i
                                            class="fas fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    `;
};

const deleteHadiah = (id) => {
    SW.show({
        title: "Peringatan !",
        text: "Apakah anda yakin ingin menghapus hadiah tersebut .?",
        icon: "warning",
        showCancelButton: true,
    }).then((res) => {
        if (res.value) {
            $.ajax({
                url: __API_URL + "delete",
                method: "DELETE",
                data: {
                    _token: __TOKEN,
                    id,
                },
                beforeSend: () => {
                    NP.d();
                },
                success: function (data) {
                    SW.toast({
                        title: data.message,
                        icon: data.status == 1 ? "success" : "error",
                    });
                    getDataHadiah();
                    NP.d();
                },
            });
        }
    });
};

const cropProcessing = (input) => {
    let file = input.files[0],
        reader = new FileReader();

    reader.onloadstart = function () {
        SW.loading();
    };

    reader.onloadend = function () {
        imgCrop.src = reader.result;
        cropImage.destroy();
        cropImage = new Cropper(imgCrop, opCropImg);
        imgCrop.value = null;
        SW.close();
        $("#modal-crop").modal("show");
    };

    reader.readAsDataURL(file);
};

const init = () => {
    cropImage = new Cropper(imgCrop, opCropImg);
    getDataHadiah();
};

$(() => {
    init();

    $("#btn-pick-img").click(() => {
        $("#form-img").click();
    });

    $("#btn-pick-img-edit").click(() => {
        $("#edit-form-img").click();
    });

    $("#form-img").change(() => {
        let input = document.getElementById("form-img");
        cropProcessing(input);
    });

    $("#edit-form-img").change(() => {
        let input = document.getElementById("edit-form-img");
        cropProcessing(input);
    });

    $("#rotateLeft").on("click", function () {
        cropImage.rotate(-90);
    });

    $("#rotateRight").on("click", function () {
        cropImage.rotate(90);
    });

    $("#reset").on("click", function () {
        cropImage.reset();
    });

    $("#modal-crop").on("hide.bs.modal", function () {
        $("#modal-add-hadiah").css({
            overflow: "auto",
        });

        $("#modal-edit-hadiah").css({
            overflow: "auto",
        });

        $("body").css({
            overflow: "hidden",
        });
    });

    $("#modal-edit-hadiah").on("show.bs.modal", function () {
        idImgPreview = "#edit-img-preview";
        idImg = "#edit-gambar";
        $("body").css({
            overflow: "hidden",
        });
    });

    $("#modal-add-hadiah").on("show.bs.modal", function () {
        idImgPreview = "#img-preview";
        idImg = "#gambar";
        $("body").css({
            overflow: "hidden",
        });
    });

    $("#modal-add-hadiah, #modal-edit-hadiah").on("hide.bs.modal", function () {
        $("body").css({
            overflow: "auto",
        });
    });

    $("#getCropedImg").on("click", function () {
        SW.loading();
        var res = cropImage
            .getCroppedCanvas({
                width: 160,
                height: 90,
                minWidth: 256,
                minHeight: 256,
                maxWidth: 4096,
                maxHeight: 4096,
                fillColor: "#fff",
                imageSmoothingEnabled: false,
                imageSmoothingQuality: "high",
            })
            .toDataURL();

        $(idImgPreview).attr("src", res);
        $(idImgPreview).removeClass("d-none");
        $(idImg).val(res);

        $("#modal-crop").modal("hide");

        setTimeout(function () {
            SW.close();
        }, 1000);
    });

    $("#form-add-hadiah").submit(() => {
        let dataAdd = $("#form-add-hadiah").serializeToJSON();

        $.ajax({
            url: __API_URL + "add",
            method: "POST",
            data: {
                ...dataAdd,
                _token: __TOKEN,
            },
            beforeSend: function () {
                NP.s();
            },
            success: function (data) {
                if (data.status == 0) {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "error",
                    });
                } else {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "success",
                    });

                    getDataHadiah();
                    $("#modal-add-hadiah").modal("hide");
                    $("#img-preview").addClass("d-none");
                    kosongi();
                }

                NP.d();
            },
        });
        console.log(dataAdd);

        return false;
    });

    $("#btn-edit-hadiah").click(function () {
        let nama = $("#edit-nama").val(),
            poin = $("#edit-poin").val(),
            stok = $("#edit-stok").val(),
            gambar = $("#edit-gambar").val(),
            id = $("#edit-id").val();

        let sendData = {
            _token: __TOKEN,
            nama,
            poin,
            stok,
            gambar,
            id,
        };

        $.ajax({
            url: __API_URL + "edit",
            method: "PUT",
            data: sendData,
            beforeSend: function () {
                NP.s();
            },
            success: function (data) {
                if (data.status == 0) {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "error",
                    });
                } else {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "success",
                    });

                    getDataHadiah();
                    $("#modal-edit-hadiah").modal("hide");
                }

                NP.d();
            },
        });
    });

    $("#modal-add-hadiah").on("shown.bs.modal", () => {
        $("#nama").focus();
    });

    $("#modal-edit-hadiah").on("shown.bs.modal", () => {
        $("#edit-nama").focus();
    });
});

function kosongi(type = "") {
    $("#" + type + "nama").val("");
    $("#" + type + "poin").val("");
    $("#" + type + "stok").val("");
    $("#" + type + "gambar").val("");
}

function editHadiah(id) {
    $.ajax({
        url: __API_URL + "get-single",
        method: "POST",
        data: {
            _token: __TOKEN,
            id,
        },
        beforeSend: function () {
            NP.s();
        },
        success: function (data) {
            if (data.status == 1) {
                let node = data.data;
                $("#edit-id").val(node.id);
                $("#edit-nama").val(node.nama);
                $("#edit-poin").val(node.poin);
                $("#edit-stok").val(node.stok);
                $("#edit-img-preview").attr("src", __BASE_URL + node.img);
                $("#modal-edit-hadiah").modal("show");
            } else {
                SW.toast({
                    title: data.message,
                    icon: "error",
                });
            }
            NP.d();
        },
    });
}
