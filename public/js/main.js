const SW = {
    show(opt) {
        return Swal.fire(opt);
    },
    toast(opt) {
        return SW.show({
            ...opt,
            toast: true,
            timer: 2000,
            showConfirmButton: false,
            position: "top-right",
        });
    },
    loading() {
        return SW.show({
            title: "Memperoses ...",
            onBeforeOpen: () => {
                Swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false,
        });
    },
    close() {
        return Swal.close();
    },
};

const NP = {
    s() {
        NProgress.start();
    },
    d() {
        NProgress.done();
    },
};

const __TOKEN = $("meta[name=_token]").attr("content");
const __BASE_URL = $("meta[name=_base_url]").attr("content");
const __API_URL = $("meta[name=_api_url]").attr("content");

$(() => {
    $('[data-toggle="tooltip"]').tooltip();

    $("#logout-button").click(() => {
        SW.show({
            title: "Peringatan !",
            text: "Apakah anda yakin ingin keluar .?",
            icon: "warning",
            showCancelButton: true,
        }).then((res) => {
            if (res.value) {
                redirect("/logout");
            }
        });
    });
});

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

(function ($) {
    $.fn.serializeToJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || "");
            } else {
                o[this.name] = this.value || "";
            }
        });
        return o;
    };
})(jQuery);

const redirect = (to) => {
    window.location.href = to;
};

const LS = {
    get(item) {
        return localStorage.getItem(item) ?? null;
    },
    set(key, value) {
        localStorage.setItem(key, value);
    },
    remove(key) {
        localStorage.removeItem(key);
    },
};

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}

(function ($) {
    $.fn.inputFilter = function (inputFilter) {
        return this.on(
            "input keydown keyup mousedown mouseup select contextmenu drop",
            function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(
                        this.oldSelectionStart,
                        this.oldSelectionEnd
                    );
                } else {
                    this.value = "";
                }
            }
        );
    };
})(jQuery);
