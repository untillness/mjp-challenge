const init = () => {
    getDataTransaksi();
    getDetail();
};

const getDataTransaksi = () => {
    $.ajax({
        url: __API_URL + "get-transaksi",
        method: "GET",
        beforeSend: () => {
            NP.s();
            $("#accordion-transaksi").html(`
                <div class="card card-primary">
                    <div class="card-body">
                        Memuat ...
                    </div>
                </div>
            `);
        },
        success: (res) => {
            if (res.data.length > 0) {
                $("#accordion-transaksi").html(
                    buildCollabsibleTransaksi(res.data)
                );
            } else {
                $("#accordion-transaksi").html(`
                    <div class="callout callout-primary">
                            Tidak ada transaksi sebelumnya !
                    </div>
                `);
            }

            NP.d();
        },
    });
};

const getDataHadiah = () => {
    $.ajax({
        method: "GET",
        url: __API_URL + "get-hadiah",
        beforeSend: () => {
            let element = "";
            element = `
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    Memuat ....
                                </div>
                            </div>
                        </div>
                    `;
            $("#daftar-hadiah").html(element);

            NP.s();
        },
        success: function (data) {
            if (data.status == 1) {
                let dataHadiah = data.data.dataHadiah;
                let element = "";

                if (dataHadiah.length > 0) {
                    dataHadiah.forEach((node, i) => {
                        element += buildCard(node, data.data.poin);
                    });
                } else {
                    element = `
                        <div class="col-12">
                            <div class="callout callout-info">Tidak ada hadiah</div>
                        </div>
                    `;
                }
                $("#daftar-hadiah").html(element);
            }

            NP.d();
        },
    });
};

const buildCard = (node, poin = "", type = "", created_at = "") => {
    return `
        <div class="col-12 col-sm-6">
            <div class="card elevation-0"
                style="height: 150px; background: url('${
                    __BASE_URL + node.img
                }') center center no-repeat">
            </div>
            <div class="card scale-card custom-card">
                <div class="card-body">
                    <h4>${node.nama}</h4>
                    ${
                        type != "kupon"
                            ? `
                                <span class="badge badge-pill badge-primary">${node.poin} POIN</span>
                                <span class="badge badge-pill badge-success">STOK ${node.stok}</span>
                            `
                            : `
                                <span>Waktu penukaran : <span class="badge badge-pill badge-primary">${created_at}</span></span>
                            `
                    }
                </div>
                ${
                    type != "kupon"
                        ? `
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" ${
                                    poin - node.poin < 0
                                        ? 'disabled data-toggle="tooltip" data-placement="right" title="Tooltip on right"'
                                        : "onclick='tukarHadiah(" +
                                          node.id +
                                          ", " +
                                          node.poin +
                                          ")'"
                                }>TUKAR HADIAH</button>
                            </div>
                        `
                        : ""
                }
            </div>
        </div>
    `;
};

const tukarHadiah = (id, poin) => {
    SW.show({
        title: "Informasi !",
        html: `Apakah anda yakin ingin menukarkan ${poin} poin anda degan hadiah ini .?`,
        icon: "info",
        showCancelButton: true,
    }).then((res) => {
        if (res.value) {
            processTukarHadiah(id);
        }
    });
};

const processTukarHadiah = (id) => {
    $.ajax({
        url: __API_URL + "tukar",
        method: "POST",
        data: {
            _token: __TOKEN,
            id,
        },
        beforeSend: () => {
            SW.loading();
        },
        success: (res) => {
            if (res.status == 1) {
                SW.toast({
                    title: res.message,
                    icon: "success",
                });
                getDataHadiah();
            } else {
                SW.toast({
                    title: res.message,
                    icon: "error",
                });
            }

            getDetail();
        },
    });
};

const buildCollabsibleTransaksi = (data) => {
    let element = "";

    data.forEach((node) => {
        let dataTransaksi = JSON.parse(node.detail);

        let elementTr = "";

        dataTransaksi.forEach((nodeTransaksi) => {
            elementTr += `
                    <tr id="transaksi-${node.id}">
                        <td>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" style="width: 20px" disabled value="${nodeTransaksi.banyak}">
                            </div>
                        </td>
                        <td>

                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" style="width: 250px" disabled value="${nodeTransaksi.nama}">
                            </div>
                        </td>
                        <td>

                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" style="width: 150px" disabled value="${nodeTransaksi.harga}">
                            </div>
                        </td>
                        <td>

                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" style="width: 150px" disabled value="${nodeTransaksi.jumlah}">
                            </div>
                        </td>
                    </tr>
                `;
        });

        element += `
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion-transaksi" href="#collapseOne-${
                                node.id
                            }"
                                class="" aria-expanded="false">
                                ${node.created_at}
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne-${
                        node.id
                    }" class="panel-collapse in collapse" style="">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Banyak</th>
                                            <th>Nama Barang</th>
                                            <th>Harga Barang</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-transaksi-${node.id}">
                                        ${elementTr}
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Total</td>
                                            <td>${formatRupiah(
                                                node.total,
                                                "Rp. "
                                            )}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Bayar</td>
                                            <td>${formatRupiah(
                                                node.bayar,
                                                "Rp. "
                                            )}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Kembalian</td>
                                            <td>${formatRupiah(
                                                node.kembali,
                                                "Rp. "
                                            )}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            `;
    });

    return element;
};

const getDetail = () => {
    $.ajax({
        url: __API_URL + "detail",
        method: "GET",
        beforeSend: () => {
            NP.s();
        },
        success: (res) => {
            $("#poin").html(res.data.poin);
            $("#transaksi").html(res.data.transaksi);
            NP.d();
        },
    });
};

const getDataKuponHadiah = () => {
    $.ajax({
        method: "GET",
        url: __API_URL + "get-kupon-hadiah",
        beforeSend: () => {
            let element = "";
            element = `
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    Memuat ....
                                </div>
                            </div>
                        </div>
                    `;
            $("#daftar-kupon-hadiah").html(element);

            NP.s();
        },
        success: function (data) {
            if (data.status == 1) {
                let dataHadiah = data.data;
                let element = "";

                if (dataHadiah.length > 0) {
                    dataHadiah.forEach((node, i) => {
                        element += buildCard(
                            JSON.parse(node.hadiah),
                            "",
                            "kupon",
                            node.created_at
                        );
                    });
                } else {
                    element = `
                        <div class="col-12">
                            <div class="callout callout-info">Tidak ada hadiah</div>
                        </div>
                    `;
                }
                $("#daftar-kupon-hadiah").html(element);
            }

            NP.d();
        },
    });
};

$(() => {
    init();

    $("#riwayat-transasi-tab-tab").click(() => {
        getDataTransaksi();
    });

    $("#tukar-poin-tab-tab").click(() => {
        getDataHadiah();
    });

    $("#kupon-hadiah-tab-tab").click(() => {
        getDataKuponHadiah();
    });
});
