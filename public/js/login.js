$(() => {
    $("#form-login").submit(() => {
        $.ajax({
            url: __API_URL,
            method: "POST",
            data: {
                ...$("#form-login").serializeToJSON(),
                _token: __TOKEN,
            },
            beforeSend: () => {
                NP.s();
            },
            success: (data) => {
                if (data.status == 0) {
                    SW.toast({
                        title: data.message,
                        icon: "error",
                    });
                } else {
                    SW.show({
                        title: "Berhasil !",
                        text: data.message,
                        icon: "success",
                    }).then(() => {
                        redirect("/");
                    });
                }

                NP.d();
            },
        });

        return false;
    });
});
