const init = () => {
    // Format mata uang.
};

const setNew = () => {
    let data = [
        {
            id: 1,
            banyak: "",
            nama: "",
            harga: "",
            jumlah: "",
        },
    ];
    LS.set("daftar-transaksi", JSON.stringify(data));

    return data;
};

const addNew = () => {
    let data = JSON.parse(LS.get("daftar-transaksi"));

    if (data == null || data == "") {
        let data = setNew();
        return data;
    }

    let lastData = data[data.length - 1];
    let idRow = lastData.id + 1;

    data.push({
        id: idRow,
        banyak: "",
        nama: "",
        harga: "",
        jumlah: "",
    });

    LS.set("daftar-transaksi", JSON.stringify(data));
    return data;
};

const generateFieldTransaksi = (data) => {
    const element = (node, index) => `
        <tr id="transaksi-${node.id}">
        <form id="form-transaksi-${node.id}"></form>
        <input type='hidden' name="id" value="${node.id}" form="form-transaksi-${node.id}"">
            <td>
                <div class="input-group input-group-sm">
                    <input data-type="input-transaksi" type="number" min="1" class="form-control" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-sm" style="width: 10px" name="banyak" data-id="${node.id}" form="form-transaksi-${node.id}" value="${node.banyak}" id="banyak-${node.id}">
                </div>
            </td>
            <td>

                <div class="input-group input-group-sm">
                    <input data-type="input-transaksi" type="text" class="form-control" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-sm" style="width: 300px" name="nama" data-id="${node.id}" form="form-transaksi-${node.id}" value="${node.nama}">
                </div>
            </td>
            <td>

                <div class="input-group input-group-sm">
                <input data-type="input-transaksi" type="number" min="1" class="form-control" aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-sm" style="width: 150px" name="harga" data-id="${node.id}" form="form-transaksi-${node.id}" value="${node.harga}">
                </div>
            </td>
            <td>

                <div class="input-group input-group-sm">
                    <input data-type="input-transaksi" type="number" min="1" class="form-control" aria-label="Sizing example input"
                        aria-describedby="inputGroup-sizing-sm" style="width: 150px" name="jumlah" data-id="${node.id}" form="form-transaksi-${node.id}" value="${node.jumlah}" readonly id="jumlah-transaksi-${node.id}">
                </div>
            </td>
            <td>
                <button class="btn btn-sm btn-danger" onClick="deleteTrasaksi(${node.id})">
                    <i class="fas fa-times"></i>
                </button>
            </td>
        </tr>
    `;

    let createEl = "";
    data.forEach((node, index) => {
        createEl += element(node, index);
    });

    return createEl;
};

const initTransaksi = () => {
    let getDaftarTransaksi = JSON.parse(LS.get("daftar-transaksi"));
    let data = null;
    if (getDaftarTransaksi == null || getDaftarTransaksi == "") {
        data = setNew();
    } else {
        data = JSON.parse(LS.get("daftar-transaksi"));
    }
    $("#table-transaksi").html(generateFieldTransaksi(data));
    writeSummary(data);
};

const controllerTransaksi = {};

$(() => {
    // init();
    initTransaksi();
    loadDataPelanggan();

    $("#btn-add-field").on("click", () => {
        let data = addNew();
        $("#table-transaksi").html(generateFieldTransaksi(data));
        $("#banyak-" + data[data.length - 1].id).focus();
    });

    $("body").on("keyup change", "input", (e) => {
        doChange(e);
    });

    $("#bayar").inputFilter(function (value) {
        return /^\d*$/.test(value); // Allow digits only, using a RegExp
    });

    $("#bayar, #kembali").on("keyup change", () => {
        doChange();
    });

    $("#pelanggan").on("click focus", () => {
        $("#modal-pelanggan").modal("show");
    });

    $("#cari-pelanggan").on("keyup change", () => {
        loadDataPelanggan($("#cari-pelanggan").val());
    });

    $("#modal-pelanggan").on("shown.bs.modal", () => {
        $("#cari-pelanggan").focus();
    });

    $("#btn-checkout").on("click", () => {
        let requireData = {
            total: $("#total").val(),
            banyak: $("#banyak").val(),
            bayar: $("#bayar").val(),
            kembali: $("#kembali").val(),
            nama: $("#pelanggan").val(),
            idPelanggan: $("#id-pelanggan").val(),
        };

        let dataTransaksi = JSON.parse(LS.get("daftar-transaksi"));

        Object.keys(requireData).forEach(function (key) {
            if (requireData[key] == "" || requireData[key] == null) {
                SW.toast({
                    title: "Ada yang belum di isi, coba cek kembali !",
                    icon: "error",
                });
                throw "gagal";
            }
        });

        dataTransaksi.forEach((node, i) => {
            Object.keys(node).forEach(function (key) {
                if (node[key] == "" || node[key] == null) {
                    SW.toast({
                        title: "Ada yang belum di isi, coba cek kembali !",
                        icon: "error",
                    });
                    throw "gagal";
                }
            });
        });

        const finalDataTransaksi = {
            ...requireData,
            data: dataTransaksi,
            _token: __TOKEN,
        };

        $.ajax({
            url: __API_URL + "checkout",
            method: "POST",
            data: finalDataTransaksi,
            beforeSend: () => {
                SW.loading();
                NP.s();
            },
            success: function (data) {
                clearTransaksi();
                SW.show({
                    title: "Transaksi berhasil !",
                    html: data.message,
                    icon: "success",
                }).then(() => {
                    SW.loading();
                    window.location.reload();
                });
                NP.d();
            },
        });
    });
});

const deleteTrasaksi = (id) => {
    let data = JSON.parse(LS.get("daftar-transaksi"));
    $("#transaksi-" + id).remove();

    let index = searchId(data, id);

    data.splice(index, 1);
    LS.set("daftar-transaksi", JSON.stringify(data));

    writeSummary(data);
    writeKembali();
};

const searchId = (arr, val) => {
    let obj = arr.find((x) => x.id == val);
    let index = arr.indexOf(obj);
    return index;
};

const doChange = (e = null) => {
    if (e != null) {
        if (e.target.dataset.type == "input-transaksi") {
            let dataId = e.target.dataset.id;
            let isiRow = $("#form-transaksi-" + dataId).serializeToJSON();
            let hasilKali = isiRow.banyak * isiRow.harga;
            isiRow.jumlah = hasilKali;

            $("#jumlah-transaksi-" + dataId).val(hasilKali);
            let data = JSON.parse(LS.get("daftar-transaksi"));

            let index = searchId(data, isiRow.id);

            data[index] = isiRow;
            LS.set("daftar-transaksi", JSON.stringify(data));

            writeSummary(data);
        }
    }

    writeKembali();
    let kembalian = $("#kembali").val();
    let bayar = $("#bayar").val();

    if (kembalian >= 0 && bayar > 0) {
        $("#btn-checkout").removeAttr("disabled");
    } else {
        $("#btn-checkout").attr("disabled", "");
    }
};

const writeSummary = (data) => {
    let jumlah = 0,
        banyak = 0;

    data.forEach((node) => {
        if (node.jumlah == "" || node.jumlah == 0) {
            return true;
        }
        jumlah = jumlah + parseInt(node.jumlah);
        banyak = banyak + parseInt(node.banyak);
    });

    $("#display-total").html(formatRupiah(jumlah.toString(), "Rp. "));
    $("#total").val(jumlah);
    $("#banyak").val(banyak);
};

const hitungTotal = () => {
    let data = JSON.parse(LS.get("daftar-transaksi"));

    let jumlah = 0,
        banyak = 0;

    data.forEach((node) => {
        if (node.jumlah == "" || node.jumlah == 0) {
            return true;
        }
        jumlah = jumlah + parseInt(node.jumlah);
        banyak = banyak + parseInt(node.banyak);
    });

    return {
        jumlah,
        banyak,
    };
};

const writeKembali = () => {
    let bayar = $("#bayar").val();
    let total = hitungTotal().jumlah;

    $("#kembali").val(bayar - total);
};

const loadDataPelanggan = (key = "") => {
    $.ajax({
        url: __API_URL + "get",
        method: "POST",
        data: {
            _token: __TOKEN,
            key,
        },
        beforeSend: () => {
            NP.s();
        },
        success: function (res) {
            let element = "";
            if (res.data.length > 0) {
                res.data.forEach((node) => {
                    element += `
                        <div class="row">
                            <div class="col-12">
                                <div class="card" style="cursor: pointer" onclick="setPelanggan('${
                                    node.id
                                }', '${escape(node.nama)}')">
                                    <div class="card-body">
                                        <h4 class="m-0">${node.nama}</h4>
                                        <p class="m-0 text-muted">${
                                            node.username
                                        }</p>
                                        <div class="badge badge-primary">${
                                            node.poin
                                        } POIN</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    `;
                });
            }

            element += `

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="btn btn-sm btn-success btn-block" onclick="addPelanggan()">Buat pelanggan baru</button>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            $("#data-pelanggan").html(element);
            NP.d();
        },
    });
};

const setPelanggan = (id, nama) => {
    $("#pelanggan").val(unescape(nama));
    $("#id-pelanggan").val(id);

    $("#modal-pelanggan").modal("hide");
};

const addPelanggan = () => {
    if (
        $("#cari-pelanggan").val() == "" ||
        $("#cari-pelanggan").val() == null
    ) {
        return SW.toast({
            title: "Upps, ada error !",
            icon: "error",
        });
    }
    SW.loading();

    let dataAdd = {
        _token: __TOKEN,
        nama: $("#cari-pelanggan").val(),
        type: "add-from-dashboard",
    };

    $.ajax({
        url: __API_URL + "add-pelanggan",
        method: "POST",
        data: dataAdd,
        beforeSend: () => {
            NP.s();
        },
        success: function (res) {
            SW.show({
                title: "Berhasil !",
                text: res.message,
                html: res.data.message,
                icon: "success",
            }).then(() => {
                setPelanggan(res.data.id, res.data.nama);
            });
            NP.d();
        },
    });
};

const clearTransaksi = () => {
    LS.remove("daftar-transaksi");
};
