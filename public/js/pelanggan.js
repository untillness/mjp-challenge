const init = () => {};

let TABLEPELANGGAN;

$(() => {
    TABLEPELANGGAN = $("#table-pelanggan").DataTable({
        autoFill: false,
        ajax: __API_URL + "get-all",
        processing: true,
        serverSide: true,
        columns: [
            {
                data: "nama",
                name: "nama",
            },
            {
                data: "username",
                name: "username",
            },
            {
                data: "poin",
                name: "poin",
            },
            {
                data: "created_at",
                name: "created_at",
            },
            {
                data: null,
                name: "aksi",
                render: function (data) {
                    let output;
                    output =
                        `
                        <button class="btn btn-sm btn-danger" onclick="deletePelanggan(` +
                        data.id +
                        `)"><i class="fas fa-trash"></i></button>
                        <button class="btn btn-sm btn-success" onclick="editPelanggan(` +
                        data.id +
                        `)"><i class="fas fa-edit"></i></button>
                    `;

                    return output;
                },
            },
        ],
    });

    init();

    $("#form-add-pelanggan").submit(function () {
        let sendData = {
            _token: __TOKEN,
            ...$("#form-add-pelanggan").serializeToJSON(),
        };

        $.ajax({
            url: __API_URL + "add",
            method: "POST",
            data: sendData,
            beforeSend: function () {
                NP.s();
            },
            success: function (data) {
                if (data.status == 0) {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "error",
                    });
                } else {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "success",
                    });

                    TABLEPELANGGAN.ajax.reload();
                    $("#modal-add-pelanggan").modal("hide");
                    kosongi();
                }

                NP.d();
            },
        });

        return false;
    });

    $("#btn-edit-pelanggan").click(function () {
        let nama = $("#edit-nama").val(),
            username = $("#edit-username").val(),
            password = $("#edit-password").val(),
            id = $("#edit-id").val(),
            rePass = $("#edit-re-pass").val();

        let sendData = {
            _token: __TOKEN,
            nama,
            username,
            password,
            rePass,
            id,
        };

        $.ajax({
            url: __API_URL + "edit",
            method: "PUT",
            data: sendData,
            beforeSend: function () {
                NP.s();
            },
            success: function (data) {
                if (data.status == 0) {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "error",
                    });
                } else {
                    SW.toast({
                        title: data.message.capitalize(),
                        icon: "success",
                    });

                    TABLEPELANGGAN.ajax.reload();
                    $("#modal-edit-pelanggan").modal("hide");
                }

                NP.d();
            },
        });
    });

    $("#nama").keyup(function () {
        let nama = $("#nama").val();
        str = nama.trim().replace(/\W+/g, "-").toLowerCase();
        $("#username").val(str);
    });

    $("#modal-add-pelanggan").on("shown.bs.modal", () => {
        $("#nama").focus();
    });

    $("#modal-edit-pelanggan").on("shown.bs.modal", () => {
        $("#edit-nama").focus();
    });
});

function kosongi(type = "") {
    $("#" + type + "nama").val("");
    $("#" + type + "username").val("");
    $("#" + type + "password").val("");
    $("#" + type + "re-pass").val("");
}

function deletePelanggan(id) {
    SW.show({
        title: "Perhatian !",
        text:
            "Semua data turunannya akan ikut terhapus juga ! Apakah anda ingin menghapus data ini ?",
        icon: "question",
        showCancelButton: true,
    }).then((res) => {
        if (res.value) {
            $.ajax({
                url: __API_URL + "delete",
                method: "DELETE",
                data: {
                    _token: __TOKEN,
                    id,
                },
                beforeSend: function () {
                    NP.s();
                },
                success: function (data) {
                    if (data.status == 1) {
                        SW.toast({
                            title: data.message,
                            icon: "success",
                        });
                        TABLEPELANGGAN.ajax.reload();
                    }
                    NP.d();
                },
            });
        }
    });
}

function editPelanggan(id) {
    $.ajax({
        url: __API_URL + "get-single",
        method: "POST",
        data: {
            _token: __TOKEN,
            id,
        },
        beforeSend: function () {
            NP.s();
        },
        success: function (data) {
            if (data.status == 1) {
                let node = data.data;
                $("#edit-id").val(node.id);
                $("#edit-nama").val(node.nama);
                $("#edit-username").val(node.username);
                $("#modal-edit-pelanggan").modal("show");
            } else {
                SW.toast({
                    title: data.message,
                    icon: "error",
                });
            }
            NP.d();
        },
    });
}
