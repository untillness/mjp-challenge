@php
    use App\Library\AuthMiddleware;

    if(AuthMiddleware::isLogin()){
        $level = AuthMiddleware::sessionLevel();
    }
    $currentRouteName = Route::currentRouteName();
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $__SITE_TITLE }} | MJP-CHALLENGE</title>
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="_base_url" content="{{ env('BASE_URL') }}">
    <meta name="_api_url" content="{{ env('BASE_URL').$currentRouteName.'/' }}">


    @include('template.css')
</head>

@php

    switch ($currentRouteName) {
        case 'login':
            $classBody = 'hold-transition login-page';
    break;

    default:
        $classBody = 'hold-transition sidebar-mini layout-fixed';
    break;
    }

    if($currentRouteName == 'dashboard'){
        $classBody .= ' sidebar-collapse';
    }
@endphp

<body class="{{ $classBody }}">

    @if($currentRouteName == 'login')
        @yield('main')
    @else
        <div class="wrapper">
            @if ($level == 'admin')
                @include('template.components.navbar')
                @include('template.components.sidebar')
            @endif

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" @if ($level != 'admin') style="margin-left: 0px !important" @endif>

                <!-- Content Header (Page header) -->
                @if (Route::currentRouteName() != 'dashboard')
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">{{ ucfirst(Route::currentRouteName()) }}</h1>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                @endif
                <!-- /.content-header -->


                <!-- Main content -->
                <section class="content">
                    @if ($currentRouteName == 'dashboard')
                        @if ($level == 'admin')
                            @yield('main.admin')
                        @else
                            @yield('main.pelanggan')
                        @endif
                    @else
                        @yield('main')
                    @endif
                </section>
                <!-- /.content -->

            </div>
            <!-- /.content-wrapper -->

            @include('template.components.footer')
        </div>
        <!-- ./wrapper -->
    @endif

    @include('template.js')
</body>

</html>
