@php
    $menuList = [
        [
            'label'=>'Dashboard',
            'name'=>'dashboard',
            'icon'=> 'fa-tachometer-alt',
            'link'=>'/'
        ],
        [
            'label'=>'Pelanggan',
            'name'=>'pelanggan',
            'icon'=> 'fa-users',
            'link'=>'/pelanggan'
        ],
        [
            'label'=>'Hadiah',
            'name'=>'hadiah',
            'icon'=> 'fa-gift',
            'link'=>'/hadiah'
        ],
    ]
@endphp


 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span class="brand-text font-weight-bold">MJP-Challenge</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @foreach ($menuList as $menu)
                <li class="nav-item">
                    <a href="{{$menu['link']}}" class="nav-link @if($menu['name'] == $currentRouteName) active @endif">
                        <i class="nav-icon fas {{$menu['icon']}}"></i>
                        <p>
                            {{$menu['label']}}
                        </p>
                    </a>
                </li>
            @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
