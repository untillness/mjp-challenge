  <footer class="main-footer text-sm" @if ($level !='admin' ) style="margin-left: 0px !important" @endif>
      <strong>Copyright &copy; {{ date('Y') }} <a href="#">Said</a>.</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
          {{-- <b>Version</b> 3.1.0-pre --}}
      </div>
  </footer>
