@extends('template.main')

@section('main')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
            <div class="col-12">
                <button class="btn btn-primary btn-sm" data-toggle="modal"
                    data-target="#modal-add-pelanggan">TAMBAH PELANGGAN</button>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table-pelanggan" class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Poin</th>
                                        <th>Pelanggan sejak</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-add-pelanggan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH PELANGGAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-add-pelanggan"></form>
                <div class="form-group">
                    <label for="">Nama Pelanggan</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama" form="form-add-pelanggan">
                </div>
                <div class="form-group">
                    <label for="">Username Pelanggan</label>
                    <input type="text" name="username" id="username" class="form-control" placeholder="Masukkan Username" form="form-add-pelanggan">
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Password</label>
                        <input type="password" name="password" id="password" class="form-control"
                            placeholder="Masukkan Password" form="form-add-pelanggan">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Ketik Ulang Password</label>
                        <input type="password" name="rePass" id="re-pass" class="form-control"
                            placeholder="Ketikkan Ulang Password" form="form-add-pelanggan">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn-add-pelanggan" form="form-add-pelanggan">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-edit-pelanggan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT PELANGGAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="edit-id" id="edit-id" class="form-control" readonly>
                <div class="form-group">
                    <label for="">Nama Pelanggan</label>
                    <input type="text" name="edit-nama" id="edit-nama" class="form-control" placeholder="Masukkan Nama">
                </div>
                <div class="form-group">
                    <label for="">Username Pelanggan</label>
                    <input type="text" name="edit-username" id="edit-username" class="form-control" placeholder="Masukkan Username" readonly>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Password</label>
                        <input type="password" name="edit-password" id="edit-password" class="form-control"
                            placeholder="Masukkan Password">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Ketik Ulang Password</label>
                        <input type="password" name="edit-re-pass" id="edit-re-pass" class="form-control"
                            placeholder="Ketikkan Ulang Password">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit-pelanggan">Simpan</button>
            </div>
        </div>
</div><!-- /.container-fluid -->

@endsection
