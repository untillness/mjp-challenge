@php
    use App\Library\AuthMiddleware;

@endphp
@extends('template.main')

@section('main.admin')
<div class="container-fluid pt-3">

    <div class="row">
        <div class="col-12 col-sm-9">
            <p>Daftar Belanjaan</p>
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                <th>Banyak</th>
                                <th>Nama Barang</th>
                                <th>Harga Barang</th>
                                <th>Jumlah</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table-transaksi">
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="pull-right d-flex mr-3" style="justify-content: flex-end">
                        <button class="btn btn-sm btn-success" id="btn-add-field"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .card-gr {
                background: rgb(0, 123, 255);
                background: -moz-linear-gradient(90deg, rgba(0, 123, 255, 1) 0%, rgba(122, 186, 255, 1) 56%, rgba(0, 212, 255, 0) 100%);
                background: -webkit-linear-gradient(90deg, rgba(0, 123, 255, 1) 0%, rgba(122, 186, 255, 1) 56%, rgba(0, 212, 255, 0) 100%);
                background: linear-gradient(90deg, rgba(0, 123, 255, 1) 0%, rgba(122, 186, 255, 1) 56%, rgba(0, 212, 255, 0) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#007bff", endColorstr="#00d4ff", GradientType=1);

                color: white;
            }

        </style>

        <div class="col-12 col-sm-3">
            <p>Checkout</p>
            <div class="stick">
                <div class="card card-gr elevation-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p class="m-0"><small>Total Belanja</small></p>
                                <h3 id="display-total">0</h3>
                                <input type="hidden" id="total">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card elevation-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <div class="fas fa-box-open"></div>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" readonly id="banyak">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <div class="fas fa-money-bill-wave"></div>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Membayar sejumlah ...."
                                        id="bayar">
                                </div>
                            </div>


                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <div class="fas fa-hand-holding-usd"></div>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Kembalian ...." readonly
                                        id="kembali" value="0">
                                </div>
                            </div>


                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <div class="fas fa-users"></div>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Pilih pelanggan" readonly
                                        id="pelanggan">
                                    <input type="hidden" name="" id="id-pelanggan">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <button class="btn btn-success btn-block btn-sm mb-2" disabled id="btn-checkout">
                    Checkout
                </button>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid -->

<style>
    /*******************************
* MODAL AS LEFT/RIGHT SIDEBAR
* Add "left" or "right" in modal parent div, after class="modal".
* Get free snippets on bootpen.com
*******************************/
    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 320px;
        height: 100%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
    }

    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }

    /*Left*/
    .modal.left.fade .modal-dialog {
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
        -o-transition: opacity 0.3s linear, left 0.3s ease-out;
        transition: opacity 0.3s linear, left 0.3s ease-out;
    }

    .modal.left.fade.in .modal-dialog {
        left: 0;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: 0px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 0;
    }

    /* ----- MODAL STYLE ----- */
    .modal-content {
        border-radius: 0;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }

</style>


<!-- Modal -->
<div class="modal right fade" id="modal-pelanggan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <p>Pilih Pelanggan</p>

                <div class="row">
                    <div class="col-12">
                        <div class="input-group input-group-sm mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <div class="fas fa-search"></div>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="cari-pelanggan">
                        </div>
                    </div>
                </div>

                <div id="data-pelanggan">

                </div>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

@endsection


@section('main.pelanggan')
<div class="container-fluid pt-3">
    <div class="row">
        <div class="col-sm-4 col-12">
            <div class="stick">
                <!-- Widget: user widget style 1 -->
                <div class="card card-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header text-white"
                        style="background: url('https://picsum.photos/seed/picsum/500/200?grayscale&blur=2') center center no-repeat ;">
                        <h3 class="widget-user-username text-center" style="font-weight: bold">
                            {{ AuthMiddleware::sessionData()->nama }}</h3>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <i class="fas fa-handshake"></i>
                                    <h5 class="description-header" id="transaksi">0</h5>
                                    <span class="description-text">TRANSAKSI</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <i class="fas fa-coins"></i>
                                    <h5 class="description-header" id="poin">0</h5>
                                    <span class="description-text">POIN</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4" style="cursor: pointer;" id="logout-button">
                                <div class="description-block d-flex"
                                    style="justify-content: center; align-items: center">
                                    <div>
                                        <h5 class="description-header">
                                            <i class="fas fa-power-off"></i>
                                        </h5>
                                        <span class="description-text">KELUAR</span>
                                    </div>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
        </div>
        <div class="col-12 col-sm-8">
            <div class="card card-primary card-tabs">
                <div class="card-header p-0 pt-1 stick elevation-1" style="z-index: 100; top: 0">
                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="riwayat-transasi-tab-tab" data-toggle="pill"
                                href="#riwayat-transasi-tab" role="tab" aria-controls="riwayat-transasi-tab"
                                aria-selected="true">Riwayat Transaksi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tukar-poin-tab-tab" data-toggle="pill" href="#tukar-poin-tab"
                                role="tab" aria-controls="tukar-poin-tab" aria-selected="false">Tukar Poin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="kupon-hadiah-tab-tab" data-toggle="pill" href="#kupon-hadiah-tab"
                                role="tab" aria-controls="kupon-hadiah-tab" aria-selected="false">Kupon Hadiah</a>
                        </li>
                    </ul>
                </div>

                <style>
                    .scale-card {
                        transform: scale(.96)
                    }

                    .custom-card {
                        margin-top: -35px
                    }

                </style>

                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <div class="tab-pane fade show active" id="riwayat-transasi-tab" role="tabpanel"
                            aria-labelledby="riwayat-transasi-tab-tab">
                            <div id="accordion-transaksi">
                                <div class="card">
                                    <div class="card-body">Memuat ...</div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tukar-poin-tab" role="tabpanel"
                            aria-labelledby="tukar-poin-tab-tab">
                            <div class="row" id="daftar-hadiah">

                            </div>
                        </div>
                        <div class="tab-pane fade" id="kupon-hadiah-tab" role="tabpanel"
                            aria-labelledby="kupon-hadiah-tab-tab">
                            <div class="row" id="daftar-kupon-hadiah">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>
@endsection
