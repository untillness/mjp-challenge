@extends('template.main')

@section('main')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-12">
            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-hadiah">TAMBAH
                HADIAH</button>
        </div>

    </div>
    <div class="row mt-3" id="daftar-hadiah">

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-add-hadiah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH HADIAH</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-add-hadiah"></form>
                <div class="form-group">
                    <label for="">Nama Hadiah</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama hadiah" form="form-add-hadiah">
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">Stok Awal</label>
                        <input type="number" name="stok" id="stok" class="form-control"
                            placeholder="Masukkan stok hadiah" form="form-add-hadiah">
                    </div>
                    <div class="form-group col-6">
                        <label for=''>Poin</label>
                        <input type="number" name="poin" id="poin" class="form-control"
                            placeholder="Masukkan poin yang diperluakan" form="form-add-hadiah">
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" id="btn-pick-img">
                        <i class="fas fa-upload mr-1"></i>Pilih Gambar
                    </button>
                    <input type="file" id="form-img" class="d-none" accept="image/*">
                    <input type="hidden" name="gambar" id="gambar" form="form-add-hadiah">
                </div>
                <div class="form-group">
                    <img src="" alt="" id="img-preview" class="img-responsive img-fluid d-none img-thumbnail">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-sm" id="btn-add-hadiah" form="form-add-hadiah">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-edit-hadiah" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT HADIAH</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="edit-id" id="edit-id" class="form-control" readonly>
                <div class="form-group">
                    <label for="">Nama Hadiah</label>
                    <input type="text" name="edit-nama" id="edit-nama" class="form-control" placeholder="Masukkan nama hadiah">
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">Stok Awal</label>
                        <input type="number" name="edit-stok" id="edit-stok" class="form-control"
                            placeholder="Masukkan stok hadiah">
                    </div>
                    <div class="form-group col-6">
                        <label for=''>Poin</label>
                        <input type="number" name="edit-poin" id="edit-poin" class="form-control"
                            placeholder="Masukkan poin yang diperluakan">
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" id="btn-pick-img-edit">
                        <i class="fas fa-upload mr-1"></i>Pilih Gambar
                    </button>
                    <input type="file" id="edit-form-img" class="d-none" accept="image/*">
                    <input type="hidden" name="edit-gambar" id="edit-gambar">
                </div>
                <div class="form-group">
                    <img src="" alt="" id="edit-img-preview" class="img-responsive img-fluid img-thumbnail">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit-hadiah">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-crop" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Wrap the image or canvas element with a block element (container) -->
                <div>
                    <img id="img-crop" src=""
                        style="max-width:100% !important; display: block">
                </div>
                <div style="display: flex; align-items: center; justify-content: space-between">

                    <div class="btn-group mt-3">
                        <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top"
                            title="Putar ke kiri" id="rotateLeft">
                            <i class="fas fa-undo-alt"></i>
                        </button>
                        <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top"
                            title="Reset gambar" id="reset">
                            <i class="fas fa-sync"></i>
                        </button>
                        <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top"
                            title="Putar ke kanan" id="rotateRight">
                            <i class="fas fa-redo-alt"></i>
                        </button>
                    </div>
                    <div class="btn-group mt-3">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                            <i class="fas fa-times"></i>
                        </button>
                        <button class="btn btn-success btn-sm" id="getCropedImg">
                            <i class="fas fa-check"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    @endsection
