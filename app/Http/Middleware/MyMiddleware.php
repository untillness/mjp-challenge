<?php

namespace App\Http\Middleware;

use App\Library\MJWT;
use App\Library\MResponse;
use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $_SERVER['HTTP_X_AUTH'] ?? null;

        if (!empty($token)) {
            $decodeToken = MJWT::decodeJWT($token);
            if ($decodeToken == false) {
                return MResponse::send(0, 'Token is not valid');
            } else {
                $routeSegment = $request->segments();

                if (count($routeSegment) == 1) {
                    $blockURL = ['dashboard', 'pelanggan', 'hadiah'];
                    $arrSearch = array_search($routeSegment[0], $blockURL);
                    if ($arrSearch > 0) {
                        return MResponse::send(0, 'This route is not allowed');
                    }
                }
            }
        }
        return $next($request);
    }
}
