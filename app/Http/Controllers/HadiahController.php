<?php

namespace App\Http\Controllers;

use App\Library\AuthMiddleware;
use App\Library\MData;
use App\Library\MJWT;
use App\Library\MResponse;
use App\thadiah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class HadiahController extends AuthMiddleware
{
    public function __construct()
    {
        parent::requireLogin();
        parent::requireAdmin();
    }

    public function index()
    {
        $data['__SITE_TITLE'] = 'Hadiah';
        return view('hadiah.hadiah', $data);
    }


    public function get_all()
    {
        $data = thadiah::orderBy('created_at', 'DESC')->get();
        $makeData = MData::toObjectData($data, ['id', 'nama', 'poin', 'stok', 'deskripsi', 'img']);
        return MResponse::send(1, '', $makeData);
    }

    public function delete(Request $a)
    {
        thadiah::destroy($a->id);
        return MResponse::send(1, 'Berhasil menghapus hadiah.');
    }

    public function add(Request $a)
    {
        $validator = Validator::make($a->all(), [
            'nama' => 'required',
            'poin' => 'required|numeric',
            'gambar' => 'required',
            'stok' => 'required|numeric',
        ], [], [
            'gambar' => 'Gambar hadiah'
        ]);

        if ($validator->fails()) {
            return MResponse::sendErrorValidation($validator);
        }

        $gambarHadiah = $this->decodeImg($a->gambar, '_hadiah');

        $dataAdd = [
            'nama' => $a->nama,
            'poin' => $a->poin,
            'stok' => $a->stok,
            'img' => $gambarHadiah
        ];

        $id = thadiah::create($dataAdd);

        return MResponse::send(1, 'Berhasil menambah hadiah');
    }

    public function get_single(Request $a)
    {
        $data = thadiah::findOrFail($a->id);
        $makeData = MData::toArrayData($data, ['id', 'nama', 'deskripsi', 'poin', 'stok', 'img']);
        return MResponse::send(1, 'Berhasil mendapatkan data.', $makeData);
    }

    public function edit(Request $a)
    {
        if (empty($a->gambar)) {
            $validator = Validator::make($a->all(), [
                'nama' => 'required',
                'poin' => 'required|numeric',
                'stok' => 'required|numeric',
            ]);
        } else {
            $validator = Validator::make($a->all(), [
                'nama' => 'required',
                'poin' => 'required|numeric',
                'stok' => 'required|numeric',
                'gambar' => 'required',
            ], [], [
                'gambar' => 'Gambar hadiah'
            ]);
        }

        if ($validator->fails()) {
            return MResponse::sendErrorValidation($validator);
        }

        if (empty($a->gambar)) {
            $dataUpdate = [
                'nama' => $a->nama,
                'poin' => $a->poin,
                'stok' => $a->stok,
            ];
        } else {
            $gambarHadiah = $this->decodeImg($a->gambar, '_hadiah');

            $dataUpdate = [
                'nama' => $a->nama,
                'poin' => $a->poin,
                'stok' => $a->stok,
                'img' => $gambarHadiah
            ];
        }

        thadiah::where('id', $a->id)->update($dataUpdate);

        return MResponse::send(1, 'Berhasil mengubah data hadiah.');
    }

    public function decodeImg($src, $sufix)
    {
        if ($src == 'img/poto.png') {
            return $src;
        }

        $parse = $src;
        $exparse = explode(',', $parse);
        $exparse = base64_decode($exparse[1]);
        $imBup = imageCreateFromString($exparse);

        $namaFile = time() . $sufix;

        $img_file = 'img/hadiah/' . $namaFile . '.png';
        imagepng($imBup, $img_file, 0);

        return $img_file;
    }
}
