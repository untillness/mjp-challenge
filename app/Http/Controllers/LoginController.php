<?php

namespace App\Http\Controllers;

use App\Library\AuthMiddleware;
use App\Library\MResponse;
use App\Library\MToken;
use App\tusers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends AuthMiddleware
{
    public function __construct()
    {
        parent::requireGuest();
    }

    public function index()
    {
        $data['__SITE_TITLE'] = 'Login';
        return view('login.login', $data);
    }

    public function do_login(Request $a)
    {
        $cekUsername = tusers::where('username', $a->username)->first();

        if ($cekUsername == null) {
            return MResponse::send(0, 'Username atau password salah.');
        }

        $cekPassword = Hash::check($a->password, $cekUsername->password);

        if (!$cekPassword) {
            return MResponse::send(0, 'Username atau password salah.');
        }

        $isApi = $a->header('postman-token') ?? null;

        if (empty($isApi)) {
            Session::put([
                'isLogin' => true,
                'data' => $cekUsername
            ]);
        }

        return MResponse::send(1, 'Berhasil login, anda akan diarahkan ke halaman dashboard.', [
            'token' => $cekUsername->token
        ]);
    }
}
