<?php

namespace App\Http\Controllers;

use App\Library\AuthMiddleware;
use App\Library\MData;
use App\Library\MJWT;
use App\Library\MResponse;
use App\tpoin;
use App\tusers;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PelangganController extends AuthMiddleware
{
    public function __construct()
    {
        parent::requireLogin();
        parent::requireAdmin();
    }

    public function index()
    {
        $data['__SITE_TITLE'] = 'Pelanggan';
        return view('pelanggan.pelanggan', $data);
    }


    public function get_all()
    {
        // $data = tusers::where('idLevel', 2)->get();

        $data = DB::table('tusers')
            ->selectRaw('tusers.*, tpoins.poin')
            ->rightJoin('tpoins', 'tpoins.idUser', '=', 'tusers.id')
            ->whereRaw('tpoins.created_at IN (SELECT MAX(tpoins.created_at)FROM tpoins GROUP BY idUser) AND idLevel = 2')->get();

        $data = json_decode($data, true);
        $makeData = MData::toObjectData($data, ['id', 'nama', 'username', 'created_at', 'poin']);
        return DataTables::of($makeData)->make(true);
    }

    public function delete(Request $a)
    {
        tusers::destroy($a->id);
        return MResponse::send(1, 'Berhasil menghapus pelanggan.');
    }

    public function add(Request $a)
    {
        if ($a->type == 'add-from-dashboard') {
            $dataAwal = $a->nama;
            $time = time();
            $time = substr($time, -5);
            $password = $dataAwal . $time;

            $dataAdd['nama'] = ucwords($dataAwal);
            $dataAdd['username'] = $dataAwal . '_' . $time;
            $dataAdd['password'] = Hash::make($password);
            $dataAdd['idLevel'] = 2;
            $dataAdd['token'] = '';


            $id = tusers::create($dataAdd);

            $dataAdd['id'] = $id->id;

            $token = MJWT::generateJWT($dataAdd);

            tusers::where('id', $id->id)->update([
                'token' => $token
            ]);

            tpoin::create([
                'idUser' => $id->id,
                'poin' => 0
            ]);

            return MResponse::send(1, 'Berhasil menambah pelanggan', [
                'message' => '<div class="badge badge-primary m-1">Nama : ' . $dataAdd['nama'] . '</div><div class="badge badge-primary m-1">Username : ' . $dataAdd['username'] . '</div><div class="badge badge-primary m-1">Password : ' . $password . '</div>',
                'nama' => $dataAdd['nama'],
                'id' => $id->id,
            ]);
        }


        $validator = Validator::make($a->all(), [
            'nama' => 'required',
            'username' => 'required|alpha_dash|min:3',
            'password' => 'required|min:8',
            'rePass' => 'required|min:8|same:password',
        ], [
            'rePass.same' => 'Password konfirmasi harus sama.',
            'username.alpha_dash' => 'Username tidak boleh mengandung spasi.'
        ], [
            'rePass' => 'Password konfirmasi'
        ]);

        if ($validator->fails()) {
            return MResponse::sendErrorValidation($validator);
        }

        $cekUsername = tusers::where('username', $a->username)->count();

        if ($cekUsername > 0) {
            return MResponse::send(0, 'Username sudah terdaftar, silahkan coba yang lain.');
        }

        $dataAdd = [
            'nama' => $a->nama,
            'username' => $a->username,
            'password' => Hash::make($a->password),
            'token' => '',
            'idLevel' => 2
        ];

        $id = tusers::create($dataAdd);
        $dataAdd['id'] = $id->id;

        $token = MJWT::generateJWT($dataAdd);

        tusers::where('id', $id->id)->update([
            'token' => $token
        ]);


        tpoin::create([
            'idUser' => $id->id,
            'poin' => 0
        ]);

        return MResponse::send(1, 'Berhasil menambah pelanggan');
    }

    public function get_single(Request $a)
    {
        $data = tusers::findOrFail($a->id);
        $makeData = MData::toArrayData($data, ['id', 'nama', 'username']);
        return MResponse::send(1, 'Berhasil mendapatkan data.', $makeData);
    }

    public function edit(Request $a)
    {
        if (empty($a->password)) {
            $validator = Validator::make($a->all(), [
                'nama' => 'required',
            ]);
        } else {
            $validator = Validator::make($a->all(), [
                'nama' => 'required',
                'password' => 'required|min:8',
                'rePass' => 'required|min:8|same:password',
            ], [
                'rePass.same' => 'Password konfirmasi harus sama.',
            ], [
                'rePass' => 'Password konfirmasi'
            ]);
        }

        if ($validator->fails()) {
            return MResponse::sendErrorValidation($validator);
        }

        if (empty($a->password)) {
            $dataUpdate = [
                'nama' => $a->nama,
            ];
        } else {
            $dataUpdate = [
                'nama' => $a->nama,
                'password' => Hash::make($a->password)
            ];
        }

        tusers::where('id', $a->id)->update($dataUpdate);

        return MResponse::send(1, 'Berhasil mengubah data pelanggan.');
    }

    public function get(Request $a)
    {
        $data = DB::table('tusers')
            ->selectRaw('tusers.username, tusers.nama, tpoins.poin, tusers.id')
            ->rightJoin('tpoins', 'tpoins.idUser', '=', 'tusers.id')
            ->whereRaw('tpoins.created_at IN (SELECT MAX(tpoins.created_at) FROM tpoins GROUP BY idUser) AND idLevel = 2');

        if (!empty($a->key)) {
            $data->where([
                ['idLevel', '!=', '1'],
                ['nama', 'like', '%' . $a->key . '%'],
                ['username', 'like', '%' . $a->key . '%'],
            ]);
        }

        $data = json_decode($data->get(), true);
        $makeData = MData::toObjectData($data, ['id', 'nama', 'username', 'poin']);
        return MResponse::send(1, 'Berhasil memuat data', $makeData);
    }
}
