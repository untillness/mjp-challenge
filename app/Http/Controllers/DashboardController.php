<?php

namespace App\Http\Controllers;

use App\Library\AuthMiddleware;
use App\Library\MData;
use App\Library\MResponse;
use App\tdaftarhadiah;
use App\thadiah;
use App\tpoin;
use App\ttransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends AuthMiddleware
{
    public function __construct()
    {
        parent::requireLogin();
    }

    public function index()
    {
        $data['__SITE_TITLE'] = 'Dashboard';
        return view('dashboard.dashboard', $data);
    }

    public function do_logout()
    {
        Session::flush();
        return redirect()->route('login');
    }

    public function checkout(Request $a)
    {
        $cekPoin = tpoin::where('idUser', $a->idPelanggan)->orderBy('created_at', 'DESC')->first();

        if ($cekPoin == null) {
            $poin = 5;
        } else {
            $poin = 5 + (int)$cekPoin->poin;
        }

        $dataAdd = [
            'total' => $a->total,
            'banyak' => $a->banyak,
            'bayar' => $a->bayar,
            'kembali' => $a->kembali,
            'nama' => $a->nama,
            'idUser' => $a->idPelanggan,
            'detail' => json_encode($a->data),
        ];

        ttransaksi::create($dataAdd);
        tpoin::create([
            'poin' => $poin,
            'idUser' => $a->idPelanggan
        ]);

        return MResponse::send(1, 'Selamat, pelanggan <b>' . $a->nama . '</b> mendapatkan tambahan <b>5 Poin</b>.');
    }

    public function get_transaksi()
    {
        $data = ttransaksi::where('idUser', AuthMiddleware::sessionData()->id)->orderBy('created_at', 'DESC')->get();
        $makeData = MData::toObjectData($data, ['detail', 'total', 'bayar', 'kembali', 'created_at', 'id']);
        return MResponse::send(1, 'Berhasil ', $makeData);
    }

    public function get_hadiah()
    {
        $poin = tpoin::where('idUser', AuthMiddleware::sessionData()->id)->orderBy('created_at', 'DESC')->first();
        $data = thadiah::orderBy('poin', 'ASC')->get();
        $makeData['dataHadiah'] = MData::toObjectData($data, ['id', 'nama', 'poin', 'img', 'stok']);
        $makeData['poin'] = $poin->poin;
        return MResponse::send(1, 'Berhasil ', $makeData);
    }

    public function get_kupon_hadiah()
    {
        $data = tdaftarhadiah::where('idUser', AuthMiddleware::sessionData()->id)->orderBy('created_at', 'DESC')->get();
        $makeData = MData::toObjectData($data, ['id', 'hadiah', 'created_at']);

        return MResponse::send(1, 'Berhasil ', $makeData);
    }

    public function tukar(Request $a)
    {
        $idUser = AuthMiddleware::sessionData()->id;
        $poin = tpoin::where('idUser', $idUser)->orderBy('created_at', 'DESC')->first();
        $hadiah = thadiah::where('id', $a->id)->first();

        if ((int)$poin->poin - (int)$hadiah->poin < 0) {
            return MResponse::send(0, 'Ternyata poin anda kurang :(');
        }

        if ($hadiah->stok == 0) {
            return MResponse::send(0, 'Stok hadiah lagi kosong :(');
        }

        $poinBaru = (int)$poin->poin - (int)$hadiah->poin;
        $stokBaru = (int)$hadiah->stok - 1;

        // dd();
        tpoin::create([
            'poin' => $poinBaru,
            'idUser' => $idUser
        ]);

        thadiah::where('id', $a->id)->update(['stok' => $stokBaru]);

        tdaftarhadiah::create([
            'idUser' => $idUser,
            'hadiah' => '' . json_encode($hadiah) . ''
        ]);

        return MResponse::send(1, 'Berhasil menukar poin, terimaksih :)');
    }

    public function detail()
    {

        $idUser = AuthMiddleware::sessionData()->id;
        $poin = tpoin::where('idUser', $idUser)->orderBy('created_at', 'DESC')->first()->poin;
        $transaksi = ttransaksi::where('idUser', $idUser)->count();

        return MResponse::send(1, 'Berhasil', ['poin' => $poin, 'transaksi' => $transaksi]);
    }
}
