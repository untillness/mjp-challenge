<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tusers extends Model
{
    protected $guarded = [];

    public function tlevels()
    {
        return $this->belongsTo(tlevel::class, 'idLevel', 'id');
    }
}
