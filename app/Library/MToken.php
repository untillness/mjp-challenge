<?php

namespace App\Library;

class MToken
{
    static public function hasToken()
    {
        return empty(self::getToken()) ? false : true;
    }

    static public function getToken()
    {
        return $_SERVER['HTTP_X_AUTH'] ?? null;
    }
}
