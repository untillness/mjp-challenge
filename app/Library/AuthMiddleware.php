<?php

namespace App\Library;

use App\Http\Controllers\Controller;
use App\tusers;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use stdClass;

class AuthMiddleware extends Controller
{

    public function __construct()
    {
    }

    public function requireLogin()
    {
        if (empty(self::isLogin())) {
            return Redirect::route('login')->send();
        }
    }

    static public function requireGuest()
    {
        if (!empty(self::isLogin())) {
            return Redirect::route('dashboard')->send();
        }
    }

    public function requireAdmin()
    {
        if (self::sessionLevel() != 'admin') {
            if (MToken::hasToken()) {
                return abort(MResponse::send(0, 'Your token is not valid'));
            }
            return Redirect::route('dashboard')->send();
        }
    }

    static function getSession()
    {
        return Session::all();
    }

    static public function isLogin()
    {
        return self::getdriverSession()['isLogin'] ?? false;
    }

    static public function sessionData()
    {
        return self::getdriverSession()['data'];
    }

    static public function sessionLevel()
    {
        return self::getdriverSession()['data']->tlevels->nama;
    }

    static public function getdriverSession()
    {
        $hasToken = MToken::hasToken();
        if ($hasToken) {
            $token = MToken::getToken();
            $decodeToken = MJWT::decodeJWT($token);

            $user = tusers::find($decodeToken->data->id);
            $newSession = [
                'isLogin' => true,
                'data' => $user
            ];
            return $newSession;
        }
        return Session::all();
    }
}
