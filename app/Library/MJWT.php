<?php

namespace App\Library;

use App\tusers;
use Firebase\JWT\JWT;
use PhpParser\Node\Stmt\TryCatch;

class MJWT
{
    public function __construct()
    {
    }

    static private function getKey()
    {
        return 'key';
    }


    static public function generateJWT($data)
    {
        $now_seconds = time();
        $payload = array(
            "iss" => 'mjp-challenge',
            "sub" => 'mjp-challenge',
            "aud" => 'mjp-challenge',
            "iat" => $now_seconds,
            'data' => $data
        );

        return JWT::encode($payload, self::getKey(), "HS256");

        // return MResponse::send(0, 'Ada yang aneh, silahkan muat ulang halaman ini.');
    }

    static public function decodeJWT($token)
    {
        $cek = tusers::where('token', $token)->count();

        if ($cek == 0) {
            return false;
        }

        try {
            return JWT::decode($token, self::getKey(), ['HS256']);
        } catch (\Exception $e) {
            return false;
        }
    }
}
