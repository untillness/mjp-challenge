<?php

namespace App\Library;


class MResponse
{
    static public function send($status, $message, $data = [])
    {
        $dataresponse = [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($dataresponse);
    }

    static function sendErrorValidation($validator)
    {

        foreach ($validator->errors()->messages() as $err) {
            $error = $err[0];
            break;
        }

        return self::send(0, $error);
    }
}
