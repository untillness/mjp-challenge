<?php

namespace App\Library;

class MData
{
    static function toArrayData($data, $output)
    {
        $out = [];

        if (is_array($data)) {
            foreach ($data as $da) {
                $newArr = [];
                foreach ($output as $oo) {
                    $newArr[$oo] = $da[$oo];
                }
                array_push($out, $newArr);
            }

            return $out;
        }

        foreach ($output as $oo) {
            $type = gettype($oo);
            if ($type == 'array') {
                $explode = explode('.', $oo[1]);
                if (count($explode) > 1) {
                    $out[$oo[0]] = $data[$explode[0]][$explode[1]];
                } else {
                    $out[$oo[0]] = $data[$oo[1]];
                }
            } else {
                $explode = explode('.', $oo);
                if (count($explode) > 1) {
                    $out[$explode[1]] = $data[$explode[0]][$explode[1]];
                } else {
                    $out[$oo] = $data[$oo];
                }
            }
        }

        return $out;
    }

    static function toObjectData($data, $output)
    {
        $out = [];

        foreach ($data as $da) {
            $newArr = [];
            foreach ($output as $oo) {
                $type = gettype($oo);
                if ($type == 'array') {
                    $explode = explode('.', $oo[1]);
                    if (count($explode) > 1) {
                        $newArr[$oo[0]] = $da[$explode[0]][$explode[1]];
                    } else {
                        $newArr[$oo[0]] = $da[$oo[1]];
                    }
                } else {
                    $explode = explode('.', $oo);
                    if (count($explode) > 1) {
                        $newArr[$explode[1]] = $da[$explode[0]][$explode[1]];
                    } else {
                        if ($oo == 'created_at') {
                            $newArr[$oo] = IndoDate::make($da[$oo]);
                        } else {
                            $newArr[$oo] = $da[$oo];
                        }
                    }
                }
            }
            array_push($out, $newArr);
        }

        return $out;
    }
}
