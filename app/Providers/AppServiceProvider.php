<?php

namespace App\Providers;

use App\Library\MJWT;
use App\Library\MResponse;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        //
        $kernel = app()->make('Illuminate\Contracts\Http\Kernel');
        $kernel->pushMiddleware(\App\Http\Middleware\EncryptCookies::class);
        $kernel->pushMiddleware(\Illuminate\Session\Middleware\StartSession::class);

        $X_Auth = $_SERVER['HTTP_X_AUTH'] ?? null;

        if ($X_Auth == null) {
            $kernel->pushMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);
        }
    }
}
