<?php

use App\Library\MJWT;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Tuser_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = [
            [
                'nama' => 'admin',
                'username' => 'admin',
                'password' => Hash::make('admin123'),
                'idLevel' => 1,
                'token' => ''
            ]
        ];

        foreach ($level as $item) {
            $admin = DB::table('tusers')->insertGetId($item);

            $item['id'] = $admin;
            DB::table('tusers')->where('id', $admin)->update(['token' => MJWT::generateJWT($item)]);
        }
    }
}
