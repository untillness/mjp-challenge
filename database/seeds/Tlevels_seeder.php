<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tlevels_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = [
            [
                'nama' => 'admin',
                'display' => 'admin'
            ],
            [
                'nama' => 'user',
                'display' => 'user'
            ]
        ];

        foreach ($level as $item) {
            DB::table('tlevels')->insert($item);
        }
    }
}
