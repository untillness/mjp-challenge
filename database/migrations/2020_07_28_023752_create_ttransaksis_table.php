<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ttransaksis', function (Blueprint $table) {
            $table->id();
            $table->text('total');
            $table->text('banyak');
            $table->text('bayar');
            $table->text('kembali');
            $table->text('nama');
            $table->unsignedBigInteger('idUser');
            $table->text('detail');
            $table->timestamps();

            $table->foreign('idUser')->references('id')->on('tusers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ttransaksis');
    }
}
