<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/coba', 'Coba@index');

Route::get('/', 'DashboardController@index')->name('dashboard');

Route::group(['prefix' => 'dashboard'], function () {
    Route::post('/get', 'PelangganController@get');
    Route::post('/checkout', 'DashboardController@checkout');
    Route::post('/add-pelanggan', 'PelangganController@add');
    Route::get('/get-transaksi', 'DashboardController@get_transaksi');
    Route::get('/get-hadiah', 'DashboardController@get_hadiah');
    Route::get('/get-kupon-hadiah', 'DashboardController@get_kupon_hadiah');
    Route::post('/tukar', 'DashboardController@tukar');
    Route::get('/detail', 'DashboardController@detail');
});

Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@do_login');
Route::get('/logout', 'DashboardController@do_logout');

Route::group(['prefix' => 'pelanggan'], function () {
    Route::get('/', 'PelangganController@index')->name('pelanggan');

    Route::get('/get-all', 'PelangganController@get_all');
    Route::post('/add', 'PelangganController@add');
    Route::post('/get-single', 'PelangganController@get_single');
    Route::delete('/delete', 'PelangganController@delete');
    Route::put('/edit', 'PelangganController@edit');
});

Route::group(['prefix' => 'hadiah'], function () {
    Route::get('/', 'HadiahController@index')->name('hadiah');

    Route::get('/get-all', 'HadiahController@get_all');
    Route::post('/add', 'HadiahController@add');
    Route::post('/get-single', 'HadiahController@get_single');
    Route::delete('/delete', 'HadiahController@delete');
    Route::put('/edit', 'HadiahController@edit');
});
